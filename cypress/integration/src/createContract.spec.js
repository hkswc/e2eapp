/// <reference types="Cypress" />

const initiatorUsername = 'hampus.karlsson@swiftcourt.se'
const initiatorPassword = 'test123'

const responderUsername = 'asdasd@hotmail.com'




context('initiator create contract', () => {
  before(() => {
    cy.visit('http://localhost:3000');
  })

  beforeEach(() => {
    const clear = Cypress.LocalStorage.clear
    Cypress.LocalStorage.clear = function (keys, ls, rs) { }
  })

  after(() => {
    localStorage.clear();
  })

  it('initiator login  - click on a Login element', () => {
    cy.get("form[ng-submit='login()'] input[name=email]")
    .type(initiatorUsername)
    cy.get("form[ng-submit='login()'] input[name=password]")
    .type(initiatorPassword)
    cy.get("form[ng-submit='login()'] button[type='submit']").click()
  })

  it('.createContract() - click on a Create Contract element', () => {
    // Press the create contract button
    cy.get('ul[class="right-actions"] .btn-lightgreen').click()
    //Pick a template
    cy.get('.btn-lightgreen').click()
    //Press the kjöper
    cy.get('div[ng-transclude] .col-xs-6 .btn-lightgreen').click()
  })

  it('.fillContract() - fill the contract with nessecary information', () => {
    
    // Buyer information
    cy.get("fieldset[name='initiatorForm'] input[type='phonenumber']")
    .type('7362534261');

    cy.get("fieldset[name='initiatorForm'] input[name='address.zipCode']")
    .type(`"1234"`)

    //Seller information
    
    cy.get("fieldset[name='responderForm'] input[name='firstName']")
    .type('selgerName')
    
    cy.get("fieldset[name='responderForm'] input[name='lastName']")
    .type('selgerLastname')
    
    cy.get("fieldset[name='responderForm'] input[name='email']")
    .type(responderUsername)
    
    cy.get("fieldset[name='responderForm'] input[type='phonenumber']")
    .type('7362534261')
    
    // press the sign button from the contract 
    cy.get('a[spinner-button="savingContract"]').click()


    // Accept terms before signing
  })
  
  it('.signTheContract() - accpet the terms and sign the contract', () => {
    cy.wait(4000);
    cy.get('.fa-square-o').click();
    
    cy.get('canvas').click({ x: 4 , y : 5});
    cy.get('canvas').click({ x: 7 , y : 10});
    cy.get('button[type="submit"]').click()

  })

  it('.shareTheContract() - share the contract', () => {
    cy.get('button[ng-click="askOtherToSign()"]').click()
  })
})
